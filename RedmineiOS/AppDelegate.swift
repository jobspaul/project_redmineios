//
//  AppDelegate.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/25/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift

@UIApplicationMain
@available(iOS 10.0, *)
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var plistPathKey: String!
    var key: String? = ""
    var launchedShortcutItem: UIApplicationShortcutItem?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Account.sharedInstance.preparePlistKey()
        plistPathKey = Account.sharedInstance.key
        do {
            let pKey = try String(contentsOfFile: plistPathKey)
            key = pKey
        } catch {
            print(error)
        }
        if key != "" {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "MainController")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        } else {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "Login")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
        // keyboard
        setupKeyboard()
        // action 3D Touch
        setupShortcutItems(withOptions: launchOptions)
        UIApplication.shared.statusBarStyle = .lightContent
        // Set NavigationController Color
        UINavigationBar.appearance().tintColor = UIColor.white
        // Set NavigationController Border Color
        UINavigationBar.appearance().shadowImage = UIImage.imageWithColor(color: UIColor(red:46.0/255.0, green:55.0/255.0, blue:72.0/255.0, alpha:1))
        // Set NavigationController Background Color
        UINavigationBar.appearance().setBackgroundImage(UIImage.imageWithColor(color: UIColor(red:46.0/255.0, green:55.0/255.0, blue:72.0/255.0, alpha:1)), for: .default)
        // navigationBar
        customizeNavigationBars()
        customizeBarButtonItems()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        AppDelegateManager.sharedInstance.addCoveredView(toWindow: window)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        AppDelegateManager.sharedInstance.removeCoveredView(fromWindow: window)
        Account.sharedInstance.preparePlistKey()
        Account.sharedInstance.preparePlistId()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "RedmineiOS")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

@available(iOS 10.0, *)
extension AppDelegate {

    func setupShortcutItems(withOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        let NewIssue = UIMutableApplicationShortcutItem(
            type: Bundle.main.bundleIdentifier! + ".NewIssue",
            localizedTitle: "New Issue",
            localizedSubtitle: nil,
            icon: UIApplicationShortcutIcon(templateImageName: "new_issue"),
            userInfo:nil)
        
        UIApplication.shared.shortcutItems = [NewIssue]
        
        if let shortcutItem = launchOptions?[UIApplicationLaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
            launchedShortcutItem = shortcutItem
        }
    }
    
    func customizeNavigationBars() {
        let appearance = UINavigationBar.appearance()
        let font = UIFont(name: "Kanit-Regular", size: 18)!
        appearance.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),
            NSAttributedStringKey.font : font
        ]
    }
    
    func customizeBarButtonItems() {
        let appearance = UIBarButtonItem.appearance()
        let font = UIFont(name: "Kanit-Regular", size: 18)!
        appearance.setTitleTextAttributes(
            [
                NSAttributedStringKey.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),
                NSAttributedStringKey.font : font
            ],
            for: UIControlState.normal)
    }
    
    func setupKeyboard() {
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().shouldShowToolbarPlaceholder = false
        IQKeyboardManager.sharedManager().previousNextDisplayMode = .alwaysHide
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
    }
    
}
