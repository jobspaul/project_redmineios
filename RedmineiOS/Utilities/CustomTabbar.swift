//
//  CustomTabbar.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit

class CustomTabbar: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Kanit-Regular", size: 12)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Kanit-Regular", size: 12)!], for: .selected)
        //UITabBar.appearance().layer.borderWidth = 1.0
        //UITabBar.appearance().clipsToBounds = true
    }
    
}
