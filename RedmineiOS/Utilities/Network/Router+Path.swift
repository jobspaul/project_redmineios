//
//  Router+Path.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

extension Router {
    
    public var path: String {
        switch self {
            case .getprojects:
                return "/projects.json"
            case .getIssue:
                return "/issues.json"
            case .getIssueDetails(let params):
                return "/issues/\(params.id!).json"
            case .Login:
                return "/users/current.json"
            case .getMember(let params):
                return "/projects/\(params.id!)/memberships.json"
            case .getVersion(let params):
                return "/projects/\(params.id ?? 0)/versions.json"
        }
    }
}
