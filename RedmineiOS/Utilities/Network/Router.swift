//
//  Router.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import Alamofire

public protocol RedmineRouter: URLRequestConvertible {
    
    var url: String { get }
    var method: HTTPMethod { get }
    var path: String { get }
    var headers: [String: String]? { get }
    var parameters: [String: AnyObject]? { get }
    var responseClass: BaseModel.Type { get }
    func asURLRequest() throws -> URLRequest
    
}

public enum Router: RedmineRouter {

    case getprojects(key: String?, page: Int?)
    case getIssue(key: String?, pid: Int?, page: Int?)
    case getIssueDetails(key: String?, id: Int?, page: Int?)
    case Login(username: String?, password: String?)
    case getMember(key: String?, id: Int?, page: Int?)
    case getVersion(key: String?, id: Int?, page: Int?)
    
}

extension Router {

    public var url: String {
        return "http://redmine.nextzy.me"
    }
    
    public var headers: [String : String]? {
        switch self {
        default:
            return nil
        }
    }
    
    public var responseClass: BaseModel.Type {
        switch self {
            case .getprojects:
                return ProjectModel.self
            case .getIssue:
                return IssueModel.self
            case .getIssueDetails:
                return IssueDetailsModel.self
            case .Login:
                return LoginModel.self
            case .getMember:
                return MemberModel.self
            case .getVersion:
                return VersionModel.self
        }
    }
    
    public func asURLRequest() throws -> URLRequest {
        let baseUrl = try url.asURL()
        var urlRequest = URLRequest(url: baseUrl.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.httpBody = Data()
        switch self {
        case .getprojects, .getIssue, .getIssueDetails, .getMember, .getVersion:
                return try Alamofire.URLEncoding.default.encode(urlRequest, with: parameters)
            case .Login(let data):
                let credentialData = "\((data.username)!):\((data.password)!)".data(using: String.Encoding.utf8)!
                let base64Credentials = credentialData.base64EncodedString(options: [])
                urlRequest.addValue("Basic \((base64Credentials))", forHTTPHeaderField: "Authorization")
                return try Alamofire.URLEncoding.default.encode(urlRequest, with: parameters)
        }
    }
}

enum asd: URLRequestConvertible {
    func asURLRequest() throws -> URLRequest {
        
        return urlRequest!
    }
}
