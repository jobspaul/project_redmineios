//
//  Router+Params.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

extension Router {
    
    public var parameters: [String: AnyObject]? {
        let limit = 25
        switch self {
            case .getprojects(let params):
                let parameters = [
                    "offset": "\(params.page!*15)",
                    "limit": limit,
                    "key": params.key!
                ] as [String : Any]
                return parameters as [String: AnyObject]
            case .getIssue(let data):
            let params = [
                    "offset": "\(data.page!*15)",
                    "limit": limit,
                    "key": data.key!,
                    "project_id": data.pid!
                ] as [String : Any]
                return params as [String : AnyObject]
            case .getIssueDetails(let params):
                let journals = "journals"
                let parameters = [
                    "offset": "\(params.page!*15)",
                    "limit": limit,
                    "include": journals,
                    "key": params.key!
                ] as [String : Any]
                return parameters as [String : AnyObject]
            case .Login:
                return nil
        case .getMember(let params):
            let parameters = [
                "offset": "\(params.page!*15)",
                "limit": limit,
                "key": params.key!
                ] as [String : Any]
            return parameters as [String: AnyObject]
        case .getVersion(let params):
            let parameters = [
                "offset": "\(params.page!*15)",
                "limit": limit,
                "key": params.key!
                ] as [String : Any]
            return parameters as [String: AnyObject]
        }
    }
}
