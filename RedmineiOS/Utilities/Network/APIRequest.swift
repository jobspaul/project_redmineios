//
//  APIRequest.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import Alamofire
import Reachability

public class APIRequest {
    
    enum APIError: Error {
        case cannotBuildUrl
        case notHttpResponse
        case unauthorized
        case forbidden
        case unknow
    }
    
    public typealias completionHandler = (AnyObject?, Error?) -> Void
    
    public static func request(withRouter router: RedmineRouter, withHandler handler: @escaping completionHandler) -> Request? {
        
        return Alamofire.request(router)
            .responseJSON(completionHandler: { response in
                debugPrint(response)
                if (response.response?.statusCode == 401) {
                    ResponseHandler(JSON: nil, router: router, completionHandler: handler)
                } else if (response.response?.statusCode == 403) {
                    ResponseHandler(JSON: nil, router: router, completionHandler: handler)
                } else {
                    if response.request?.httpBody?.isEmpty == false {
                        print("========== Body =========")
                        print(try! JSONSerialization.jsonObject(with: (response.request?.httpBody)!, options: .mutableContainers))
                        print("======================")
                    }
                    switch response.result {
                    case .success(let JSON):
                        ResponseHandler(JSON: JSON as AnyObject?, router: router, completionHandler: handler)
                    case .failure(let error):
                        ResponseHandler(JSON: error as AnyObject?, router: router, completionHandler: handler)
                        print(error)
                    }
                }
            })
    }
    
    public static func ResponseHandler(JSON: AnyObject?, router: RedmineRouter, completionHandler: APIRequest.completionHandler) {
        var instance: BaseModel? = nil
        if let JSON = JSON {
            instance = router.responseClass.init(withDictionary: JSON)
            completionHandler(instance!, nil)
        }
        completionHandler(nil, APIError.unknow)
    }

}
