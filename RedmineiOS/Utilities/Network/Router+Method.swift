//
//  Router+Method.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import Alamofire

extension Router {
    
    public var method: Alamofire.HTTPMethod {
        switch self {
        case .getprojects, .getIssue, .getIssueDetails, .Login, .getMember, .getVersion:
            return .get
        }
    }
}

