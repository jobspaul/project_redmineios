//
//  MessageAlert.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/27/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit

class MessageAlert {
    static func show(withViewController vc : UIViewController, message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ตกลง", style: .default, handler: nil)
        alert.addAction(okAction)
        vc.present(alert, animated: true, completion: nil)
    }
}
