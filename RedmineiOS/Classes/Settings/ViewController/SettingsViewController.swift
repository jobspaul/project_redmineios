//
//  SettingsViewController.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SettingsViewController: BaseTableViewController {
    
    var pKey: String!
    var pId: String!
    var firstname: String!
    var lastname: String!
    
    lazy var viewModel: LoginViewModelProtocol = LoginViewModel()
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 && indexPath.row == 0 {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: "http://redmine.nextzy.me")!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: "http://redmine.nextzy.me")! as URL)
            }
        } else if indexPath.section == 0 && indexPath.row == 2 {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
            let titleFont = [NSAttributedStringKey.font: UIFont(name: "Kanit-Regular", size: 16.0)!]
            let messageFont = [NSAttributedStringKey.font: UIFont(name: "Kanit-Regular", size: 16.0)!]
            let titleAttrString = NSMutableAttributedString(string: "คุณต้องการออกจากระบบ", attributes: titleFont)
            let messageAttrString = NSMutableAttributedString(string: "ใช่หรือไม่ ?", attributes: messageFont)
            alert.setValue(titleAttrString, forKey: "attributedTitle")
            alert.setValue(messageAttrString, forKey: "attributedMessage")
            alert.addAction(UIAlertAction(title: "ใช่ ต้องการออกจากระบบ", style: .destructive, handler: {(action) -> Void in
                self.pKey = Account.sharedInstance.key
                self.pId = Account.sharedInstance.id
                do {
                    if FileManager.default.fileExists(atPath: self.pKey) {
                        try FileManager.default.removeItem(atPath: self.pKey)
                    } else {
                        try FileManager.default.removeItem(atPath: self.pId)
                    }
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "Login")
                    self.present(vc!, animated: true, completion: nil)
                } catch {
                    print(error)
                }
            }))
            alert.addAction(UIAlertAction(title: "ไม่ ต้องการอยู่ในระบบต่อ", style: .default, handler: {(action) -> Void in
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func setupView() {
        profileImage.clipsToBounds = true
        profileImage.layer.cornerRadius = profileImage.layer.frame.width / 2
    }
    
}
