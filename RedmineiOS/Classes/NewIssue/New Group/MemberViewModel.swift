//
//  MemberViewModel.swift
//  RedmineiOS
//
//  Created by Nattawut Nokyoo on 3/6/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol MemberViewModelProtocol {
    var success: PublishSubject<MemberModel?> { get set }
    var error: PublishSubject<String?> { get set }
    var dataMember: MemberModel? { get set }
    func getMember(pid: Int?)
}

class MemberViewModel: MemberViewModelProtocol {
    var success = PublishSubject<MemberModel?>()
    var error = PublishSubject<String?>()
    var dataMember: MemberModel?
    
    var limit: Int = 0
    var key: String? = ""
    var plistPathLoginKey: String!
    
    func getMember(pid: Int?) {
        plistPathLoginKey = Account.sharedInstance.key
        do {
            let plistKey = try String(contentsOfFile: plistPathLoginKey)
            key = plistKey
        } catch {
            print(error)
        }
        let router = Router.getMember(key: key, id: pid, page: 0)
        _ = APIRequest.request(withRouter: router, withHandler: getHandler())
    }
    
    func getHandler() -> APIRequest.completionHandler {
        return { [weak self] (response, error) in
            if let response = response as? MemberModel {
                self?.dataMember = response
                self?.success.onNext(response)
            } else {
                self?.error.onNext(error?.localizedDescription ?? "")
            }
        }
    }
}
