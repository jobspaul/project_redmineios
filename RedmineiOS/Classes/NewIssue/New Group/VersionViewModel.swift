//
//  VersionViewModel.swift
//  RedmineiOS
//
//  Created by Nattawut Nokyoo on 3/7/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol VersionViewModelProtocol {
    var success: PublishSubject<VersionModel?> { get set }
    var error: PublishSubject<String?> { get set }
    var dataVersion: VersionModel? { get set }
    func getVersion(pid: Int?)
}

class VersionViewModel: VersionViewModelProtocol {
    var success = PublishSubject<VersionModel?>()
    var error = PublishSubject<String?>()
    var dataVersion: VersionModel?
    
    var limit: Int = 0
    var key: String? = ""
    var plistPathLoginKey: String!
    
    func getVersion(pid: Int?) {
        plistPathLoginKey = Account.sharedInstance.key
        do {
            let plistKey = try String(contentsOfFile: plistPathLoginKey)
            key = plistKey
        } catch {
            print(error)
        }
        let router = Router.getVersion(key: key, id: pid, page: 0)
        _ = APIRequest.request(withRouter: router, withHandler: getHandler())
    }
    
    func getHandler() -> APIRequest.completionHandler {
        return { [weak self] (response, error) in
            if let response = response as? VersionModel {
                self?.dataVersion = response
                self?.success.onNext(response)
            } else {
                self?.error.onNext(error?.localizedDescription ?? "")
            }
        }
    }
}
