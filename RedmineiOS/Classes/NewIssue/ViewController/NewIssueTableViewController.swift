//
//  NewIssueTableViewController.swift
//  RedmineiOS
//
//  Created by Nattawut Nokyoo on 3/5/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit
import Foundation
import RxSwift
import RxCocoa

class NewIssueTableViewController: BaseTableViewController {
    
    var pid: Int? = 0
    var name: String? = ""
    
    @IBOutlet weak var projectTextField: UITextField!
    @IBOutlet weak var trackerTextField: UITextField!
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var assigneeTextField: UITextField!
    @IBOutlet weak var priorityTextField: UITextField!
    @IBOutlet weak var doneTextField: UITextField!
    @IBOutlet weak var versionTextField: UITextField!
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var dueDateTextField: UITextField!
    
    lazy var viewModelProject: ProjectViewModelProtocol = ProjectViewModel()
    lazy var viewModel: IssueViewModelProtocol = IssueViewModel()
    lazy var viewModelMember: MemberViewModelProtocol = MemberViewModel()
    lazy var viewModelVesion: VersionViewModelProtocol = VersionViewModel()
    
    var projectPickerView = UIPickerView()
    var trackerPickerView = UIPickerView()
    var statusPickerView = UIPickerView()
    var assigneePickerView = UIPickerView()
    var priorityPickerView = UIPickerView()
    var donePickerView = UIPickerView()
    var versionPickerView = UIPickerView()
    @objc var startDatePickerView = UIDatePicker()
    @objc var dueDatePickerView = UIDatePicker()
    var dataPass: ProjectModel?
    
    let disposeBag = DisposeBag()
    
    var trackerArray: [String] = ["Bug", "Feature", "User Story", "Task"]
    var statusArray: [String] = ["New", "In Progress", "Resolved", "Feedback", "Closed"]
    var priorityArray: [String] = ["Low", "Normal", "High", "Urgent", "Immediate"]
    var doneArray: [String] = ["0%", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModelProject.getListProject()
        viewModel.getIssue(pid: pid)
        viewModelMember.getMember(pid: pid)
        viewModelVesion.getVersion(pid: pid)
        createDatePicker()
        setPickerView()
        showDataDefault()
    
    }
    
    func showDataDefault() {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.locale = Locale(identifier: "ja_JP")
        projectTextField.text = name
        trackerTextField.text = trackerArray[0]
        statusTextField.text = statusArray[0]
        priorityTextField.text = priorityArray[1]
        doneTextField.text = doneArray[0]
        startDateTextField.text = "\(formatter.string(from: Date()))"
        
    }
    
    func setPickerView() {
        
        self.projectPickerView = setupPickerView()
        self.projectTextField.inputView = self.projectPickerView
       
        self.trackerPickerView = setupPickerView()
        self.trackerTextField.inputView = self.trackerPickerView
        
        self.statusPickerView = setupPickerView()
        self.statusTextField.inputView = self.statusPickerView
        
        self.assigneePickerView = setupPickerView()
        self.assigneeTextField.inputView = self.assigneePickerView
        
        self.priorityPickerView = setupPickerView()
        self.priorityTextField.inputView = self.priorityPickerView
        
        self.donePickerView = setupPickerView()
        self.doneTextField.inputView =  self.donePickerView
        
        self.versionPickerView = setupPickerView()
        self.versionTextField.inputView = self.versionPickerView
    }
    
    func setupPickerView() -> UIPickerView {
        let currentPickerView = UIPickerView()
        currentPickerView.dataSource = self
        currentPickerView.delegate = self
        return currentPickerView
    }
}

extension NewIssueTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case projectPickerView:
            let countrow = self.viewModelProject.data?.projectList.count ?? 0
            return countrow
        case trackerPickerView:
            let countrow = self.trackerArray.count
            return countrow
        case statusPickerView:
            let countrow = self.statusArray.count
            return countrow
        case assigneePickerView:
            let countrow = self.viewModelMember.dataMember?.MemberList.count ?? 0
            return countrow
        case priorityPickerView:
            let countrow = self.priorityArray.count
            return countrow
        case donePickerView:
            let countrow = self.doneArray.count
            return countrow
        case versionPickerView:
            let countrow = self.viewModelVesion.dataVersion?.VersionList.count ?? 0
            return countrow
        default:
            break
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case projectPickerView:
            let titleRow = viewModelProject.data?.projectList[row]
            return titleRow?.name
        case trackerPickerView:
            let titleRow = self.trackerArray[row]
            return titleRow
        case statusPickerView:
            let titleRow = self.statusArray[row]
            return titleRow
        case assigneePickerView:
            let titleRow = viewModelMember.dataMember?.MemberList[row]
            return titleRow?.userName
        case priorityPickerView:
            let titleRow = self.priorityArray[row]
            return titleRow
        case donePickerView:
            let titleRow = self.doneArray[row]
            return titleRow
        case versionPickerView:
            let titleRow = viewModelVesion.dataVersion?.VersionList[row]
            return titleRow?.name
        default:
            break
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case projectPickerView:
            self.projectTextField.text = viewModelProject.data?.projectList[row].name
            print("Project Data = \(String(describing: projectTextField.text))")
        case trackerPickerView:
            self.trackerTextField.text = trackerArray[row]
            print("Tracker Data = \(String(describing: trackerTextField.text))")
        case statusPickerView:
            self.statusTextField.text = statusArray[row]
        case assigneePickerView:
            self.assigneeTextField.text = viewModelMember.dataMember?.MemberList[row].userName
        case priorityPickerView:
            self.priorityTextField.text = priorityArray[row]
        case donePickerView:
            self.doneTextField.text = doneArray[row]
        case versionPickerView:
            self.versionTextField.text = viewModelVesion.dataVersion?.VersionList[row].name
        default:
            break
        }
    }
}

extension NewIssueTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                self.projectTextField.becomeFirstResponder()
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                self.trackerTextField.becomeFirstResponder()
            case 1:
                addAlert(withViewController: self, title: "Subject", message: "Input Data")
            case 2:
                addAlert(withViewController: self, title: "Description", message: "Input Data")
            default:
                break
            }
        case 2:
            switch indexPath.row {
            case 0:
                self.statusTextField.becomeFirstResponder()
            case 1:
                self.assigneeTextField.becomeFirstResponder()
                self.assigneeTextField.isHidden = false
            case 2:
                self.priorityTextField.becomeFirstResponder()
            case 3:
                self.doneTextField.becomeFirstResponder()
            case 4:
                self.startDateTextField.becomeFirstResponder()
            case 5:
                self.dueDateTextField.becomeFirstResponder()
            case 6:
                self.versionTextField.becomeFirstResponder()
                self.versionTextField.isHidden = false
            case 7:
                addAlert(withViewController: self, title: "Category", message: "Input Data")
            case 8:
                addAlert(withViewController: self, title: "Estimated time", message: "Input Data")
            case 9:
                addAlert(withViewController: self, title: "Parent task", message: "Input Data")
            default:
                break
            }
        default:
            break
        }
    }
}

extension NewIssueTableViewController {
    func addAlert (withViewController vc : UIViewController,title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let textView = UITextView()
        textView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let saveAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let controller = UIViewController()
        textView.frame = controller.view.frame
        controller.view.addSubview(textView)
        alert.setValue(controller, forKey: "contentViewController")
        
        let height: NSLayoutConstraint = NSLayoutConstraint(item: alert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 2, constant: view.frame.height * 0.5)
        alert.view.addConstraint(height)
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
    
        present(alert, animated: true, completion: nil)
    }
}

extension NewIssueTableViewController {
    
    func createDatePicker() {
        // toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.backgroundColor = .red
        
        // done button for toolbar
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([done], animated: false)
        
        startDateTextField.inputAccessoryView = toolbar
        startDateTextField.inputView = startDatePickerView
        
        dueDateTextField.inputAccessoryView = toolbar
        dueDateTextField.inputView = dueDatePickerView
        
        // format picker for date
        startDatePickerView.datePickerMode = .date
        dueDatePickerView.datePickerMode = .date
    }
    
    @objc func donePressed() {
        // format date
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.locale = Locale(identifier: "ja_JP")
        let dateString = formatter.string(from: startDatePickerView.date)
        let dateString2 = formatter.string(from: dueDatePickerView.date)
        
        startDateTextField.text = "\(dateString)"
        dueDateTextField.text = "\(dateString2)"
        self.view.endEditing(true)
    }
}
