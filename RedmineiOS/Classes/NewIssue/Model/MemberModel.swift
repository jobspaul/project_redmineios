//
//  MemberModel.swift
//  RedmineiOS
//
//  Created by Nattawut Nokyoo on 3/6/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import Foundation

class MemberModel: BaseModel {
    var id: Int?
    
    var project: String?
    var projectId: Int?
    var projectName: String?
    
    var user: String?
    var userId: Int?
    var userName: String?
    
    var roles: String?
    var rolesId: Int?
    var rolesName:String?
    
    var MemberList = [MemberModel]()
    
    required init(withDictionary dict: AnyObject) {
        super.init(withDictionary: dict)
        if let data = dict["memberships"] as? [[String: AnyObject]] {
            self.MemberList = data.map({ MemberModel.init(withDictionary: $0 as AnyObject) })
        }
        
        self.id = dict["id"] as? Int
        
        let project = dict["project"] as AnyObject
        self.projectId = project["id"] as? Int
        self.projectName = project["name"] as? String
    
        let user = dict["user"] as AnyObject
        self.userId = user["id"] as? Int
        self.userName = user["name"] as? String
        
        let roles = dict["user"] as AnyObject
        self.rolesId = roles["id"] as? Int
        self.rolesName = roles["name"] as? String
    }
}
