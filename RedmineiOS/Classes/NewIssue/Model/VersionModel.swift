//
//  VersionModel.swift
//  RedmineiOS
//
//  Created by Nattawut Nokyoo on 3/7/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import Foundation

class VersionModel: BaseModel {
    var id: Int?
    
    var project: String?
    var projectId: Int?
    var projectName: String?
    
    var name: String?
    var descriptionVersion: String?
    var status: String?
    var dueDate: String?
    var sharing: String?
    var createdOn: String?
    var updateOn: String?
    
    var VersionList = [VersionModel]()
    
    required init(withDictionary dict: AnyObject) {
        super.init(withDictionary: dict)
        if let data = dict["versions"] as? [[String: AnyObject]] {
            self.VersionList = data.map({ VersionModel.init(withDictionary: $0 as AnyObject)} )
        }
        
        self.id = dict["id"] as? Int
        
        let project = dict["project"] as AnyObject
        self.projectId = project["id"] as? Int
        self.projectName = project["name"] as? String
        
        self.name = dict["name"] as? String
        self.descriptionVersion = dict["description"] as? String
        self.status = dict["status"] as? String
        self.dueDate = dict["due_date"] as? String
        self.sharing = dict["sharing"] as? String
        self.createdOn = dict["created_on"] as? String
        self.updateOn = dict["updated_on"] as? String
    }
}
