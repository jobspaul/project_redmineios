//
//  ProjectViewController.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProjectViewController: BaseTableViewController {
    
    var objects: ProjectModel?
    let disposeBag = DisposeBag()
    lazy var viewModel: ProjectViewModelProtocol = ProjectViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.getListProject()
        setupViewModel()
        // MARK: TableViewDelegate
        self.tableView.delegate = self
        self.refreshControl?.addTarget(self, action: #selector(pullToRefresh), for: UIControlEvents.valueChanged)
        // MARK: Remove Back Title
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        UIApplication.shared.statusBarStyle = .lightContent
        
    }
    
    func setupViewModel() {
        viewModel.success.subscribe(onNext: { [weak self] (success) in
            self?.tableView.reloadData()
        }).disposed(by: disposeBag)
    }
    
    @objc func pullToRefresh() {
        viewModel.getListProject()
        if (self.refreshControl?.isRefreshing)!
        {
            self.refreshControl?.endRefreshing()
        }
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
}

extension ProjectViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    -> Int {
        return viewModel.data?.projectList.count ?? 0
    }
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = (viewModel.data?.projectList.count)! - 1
        if indexPath.row == lastElement {
            // handle your logic here to get more items, add it to dataSource and reload tableview
            
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProjectCell.identifier, for: indexPath) as! ProjectCell
        let item = viewModel.data?.projectList[indexPath.row]
        cell.projectNameLabel.text = String(describing: "# \((item?.name ?? "")!)")
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as? IssueViewController
        let item = viewModel.data?.projectList[(self.tableView.indexPathForSelectedRow?.row)!]
        vc?.pid = item?.id ?? 0
        vc?.name = item?.name ?? ""
        vc?.navigationItem.title = item?.name ?? ""
    }
}
