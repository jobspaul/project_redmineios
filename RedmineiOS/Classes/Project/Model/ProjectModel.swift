//
//  ProjectModel.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import Foundation

class ProjectModel: BaseModel {
    
    var id: Int?
    var name: String?
    var projectList = [ProjectModel]()
    
    required init(withDictionary dict: AnyObject) {
        super.init(withDictionary: dict)
        if let data = dict["projects"] as? [[String: AnyObject]] {
            self.projectList = data.map({ ProjectModel.init(withDictionary: $0 as AnyObject) })
        }
        
        self.id = dict["id"] as? Int
        self.name = dict["name"] as? String
        self.limit = dict["limit"] as? Int
        self.offset = dict["offset"] as? Int
        
    }
    
}
