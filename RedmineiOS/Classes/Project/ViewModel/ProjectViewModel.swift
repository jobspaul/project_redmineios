//
//  ProjectViewModel.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import RxSwift
import RxCocoa

protocol ProjectViewModelProtocol {
    var error: PublishSubject<String?> { get set }
    var success: PublishSubject<ProjectModel?> { get set }
    func getListProject()
    var data: ProjectModel? { get set }
}

class ProjectViewModel: ProjectViewModelProtocol {
    
    var data: ProjectModel?
    var error = PublishSubject<String?>()
    var success = PublishSubject<ProjectModel?>()
    var limit: Int = 0
    var key: String? = ""
    var plistPathLoginKey: String!
    
    func getListProject() {
        plistPathLoginKey = Account.sharedInstance.key
        do {
            let plistKey = try String(contentsOfFile: plistPathLoginKey)
            key = plistKey
        } catch {
            print(error)
        }
        let router = Router.getprojects(key: key, page: 0)
        _ = APIRequest.request(withRouter: router, withHandler: getHandler())
    }
    
    func getHandler() -> APIRequest.completionHandler {
        return { [weak self] (response, error) in
            if let response = response as? ProjectModel {
                self?.data = response
                self?.success.onNext(response)
            } else {
                self?.error.onNext(error?.localizedDescription ?? "")
            }
        }
    }
}
