//
//  LoginViewController.swift
//  RedmineiOS
//
//  Created by Nattawut Nokyoo on 2/9/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit
import LocalAuthentication

class LoginViewController: BaseViewController {

    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    lazy var viewModel: LoginViewModelProtocol = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindToView()
        setTextField()
        UIApplication.shared.statusBarStyle = .default
    }
    
    @IBAction func login(_ sender: Any) {
        viewModel.loginWith(username: usernameField.text!, password: passwordField.text!)
    }

}

extension LoginViewController {
    
    func bindToView() {
        viewModel.loginSucces = loginSucces()
        viewModel.loginFail = loginFail()
    }
    
    func loginSucces() -> ((LoginModel?) -> Void) {
        return { [weak self] account in
            self?.actionToMenu()
        }
    }
    
    func loginFail() -> ((Error) -> Void) {
        return { [weak self] error in
            MessageAlert.show(withViewController: self!, message: "ขออภัยข้อมูลไม่ถูกต้อง")
        }
    }
}

extension LoginViewController {
    
    func actionToMenu() {
        let view = self.storyboard?.instantiateViewController(withIdentifier: "MainController")
        self.present(view!, animated: true, completion: nil)
    }
    
    private func setTextField() {
        self.setTextFieldInterface(usernameField)
        self.setTextFieldInterface(passwordField)
    }
    
    private func setTextFieldInterface(_ TextField: UITextField) {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: TextField.frame.height - 1, width: TextField.frame.width, height: 1)
        bottomLine.backgroundColor = UIColor(red:245.0/255.0, green:245.0/255.0, blue:245.0/255.0, alpha:1).cgColor
        TextField.borderStyle = UITextBorderStyle.none
        TextField.layer.addSublayer(bottomLine)
    }
}
