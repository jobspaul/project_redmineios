//
//  LoginModel.swift
//  RedmineiOS
//
//  Created by Nattawut Nokyoo on 2/9/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import Foundation

class LoginModel: BaseModel {
    
    var id: Int?
    var login: String?
    var firstname: String?
    var lastname: String?
    var mail: String?
    var createdOn: String?
    var lastLogin: String?
    var apiKey: String?
    
    required init(withDictionary dict: AnyObject) {
        super.init(withDictionary: dict)
        let user = dict["user"] as AnyObject
        self.id = user["id"] as? Int
        self.login = user["login"] as? String
        self.firstname = user["firstname"] as? String
        self.lastname = user["lastname"] as? String
        self.mail = user["mail"] as? String
        self.createdOn = user["created_on"] as? String
        self.lastLogin = user["last_login_on"] as? String
        self.apiKey = user["api_key"] as? String
    }
    
}
