//
//  LoginViewModel.swift
//  RedmineiOS
//
//  Created by Nattawut Nokyoo on 2/9/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import EZSwiftExtensions

protocol LoginViewModelProtocol {
    
    var loginModel: [LoginModel] { get set }
    var loginSucces: ((LoginModel) -> Void)? { get set }
    var loginFail: ((Error) -> Void)? { get set }
    var data: LoginModel? { get set }
    func loginWith(username: String, password: String)
    
}

class LoginViewModel: LoginViewModelProtocol {
    
    var loginSucces: ((LoginModel) -> Void)?
    var loginFail: ((Error) -> Void)?
    var loginModel = [LoginModel]()
    var data: LoginModel?
    var responeId: String? = ""

    func loginWith(username: String, password: String) {
        let router = Router.Login(username: username, password: password)
        _ = APIRequest.request(withRouter: router, withHandler: getHandler())
    }
    
    func getHandler() -> APIRequest.completionHandler  {
        return { [weak self] (response, error) in
            if let respone = response as? LoginModel {
                self?.loginSucces?(respone)
                self?.data = respone
                self?.responeId = respone.id?.toString
                let fileforKey = Account.sharedInstance.key
                let fileforId = Account.sharedInstance.id
                do {
                    try respone.apiKey?.write(toFile: fileforKey, atomically: true, encoding: String.Encoding.utf8)
                    try self?.responeId?.write(toFile: fileforId, atomically: true, encoding: String.Encoding.utf8)
                } catch {
                    print(error)
                }
            } else {
                self?.loginFail?(error!)
            }
        }
    }
}
