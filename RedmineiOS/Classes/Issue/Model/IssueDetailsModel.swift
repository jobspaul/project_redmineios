//
//  IssueDetailsModel.swift
//  RedmineiOS
//
//  Created by Nattawut Nokyoo on 2/9/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import Foundation

class IssueDetailsModel: BaseModel {
    
    var id: Int?
    var subject: String?
    var descrip: String?
    var start: String?
    var due: String?
    var done: Int?
    var estimatedHours: Int?
    var create: String?
    var update: String?
    var projectId: Int?
    var projectName: String?
    var trackerId: Int?
    var trackerName: String?
    var statusId: Int?
    var statusName: String?
    var priorityId: Int?
    var priorityName: String?
    var authorId: Int?
    var authorName: String?
    var assignedId: Int?
    var assignedName: String?
    var fixedId: Int?
    var fixedName: String?
    var journalsId: Int?
    var notes: String?
    var jornalsCreated: String?
    var userId: Int?
    var userName: String?
    var property: String?
    var detailName: String?
    var oldValue: String?
    var newValue: String?
    var IssueDetails = [IssueDetailsModel]()
    
    required init(withDictionary dict: AnyObject) {
        super.init(withDictionary: dict)
        
        let issue = dict["issue"] as AnyObject
        self.id = issue["id"] as? Int
        self.subject = issue["subject"] as? String
        self.descrip = issue["description"] as? String
        self.start = issue["start_date"] as? String
        self.due = issue["due_date"] as? String
        self.done = issue["done_ratio"] as? Int
        self.estimatedHours = issue["estimated_hours"] as? Int
        self.create = issue["created_on"] as? String
        self.update = issue["updated_on"] as? String
        // PATH Project
        let project = issue["project"] as AnyObject
        self.projectId = project["id"] as? Int
        self.projectName = project["name"] as? String
        // PATH Tracker
        let tracker = issue["tracker"] as AnyObject
        self.trackerId = tracker["id"] as? Int
        self.trackerName = tracker["name"] as? String
        // PATH Status
        let status = issue["status"] as AnyObject
        self.statusId = status["id"] as? Int
        self.statusName = status["name"] as? String
        // PATH Priority
        let priority = issue["priority"] as AnyObject
        self.priorityId = priority["id"] as? Int
        self.priorityName = priority["name"] as? String
        // PARH Author
        let author = issue["author"] as AnyObject
        self.authorId = author["id"] as? Int
        self.authorName = author["name"] as? String
        // PATH Assigned_to
        let assignedto = issue["assigned_to"] as AnyObject
        self.assignedId = assignedto["id"] as? Int
        self.assignedName = assignedto["name"] as? String
        // PATH Fixed_version
        let fixed = issue["fixed_version"] as AnyObject
        self.fixedId = fixed["id"] as? Int
        self.fixedName = fixed["name"] as? String
        
    }
    
}


