//
//  IssueModel.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import Foundation

class IssueModel: BaseModel {
    
    var id: Int?
    var subject: String?
    var descrip: String?
    var start: String?
    var due: String?
    var done: Int?
    var estimatedHours: Int?
    var create: String?
    var update: String?
    var projectId: Int?
    var projectName: String?
    var trackerId: Int?
    var trackerName: String?
    var statusId: Int?
    var statusName: String?
    var priorityId: Int?
    var priorityName: String?
    var authorId: Int?
    var authorName: String?
    var assignedId: Int?
    var assignedName: String?
    var fixedId: Int?
    var fixedName: String?
    var IssueList = [IssueModel]()
    
    required init(withDictionary dict: AnyObject) {
        super.init(withDictionary: dict)
        
        if let data = dict["issues"] as? [[String: AnyObject]] {
            self.IssueList = data.map({ IssueModel.init(withDictionary: $0 as AnyObject) })
        }
        
        self.id = dict["id"] as? Int
        self.subject = dict["subject"] as? String
        self.descrip = dict["description"] as? String
        self.start = dict["start_date"] as? String
        self.due = dict["due_date"] as? String
        self.done = dict["done_ratio"] as? Int
        self.estimatedHours = dict["estimated_hours"] as? Int
        self.create = dict["created_on"] as? String
        self.update = dict["updated_on"] as? String
        // PATH Project
        let project = dict["project"] as AnyObject
        self.projectId = project["id"] as? Int
        self.projectName = project["name"] as? String
        // PATH Tracker
        let tracker = dict["tracker"] as AnyObject
        self.trackerId = tracker["id"] as? Int
        self.trackerName = tracker["name"] as? String
        // PATH Status
        let status = dict["status"] as AnyObject
        self.statusId = status["id"] as? Int
        self.statusName = status["name"] as? String
        // PATH Priority
        let priority = dict["priority"] as AnyObject
        self.priorityId = priority["id"] as? Int
        self.priorityName = priority["name"] as? String
        // PARH Author
        let author = dict["author"] as AnyObject
        self.authorId = author["id"] as? Int
        self.authorName = author["name"] as? String
        // PATH Assigned_to
        let assigned = dict["assigned_to"] as AnyObject
        self.assignedId = assigned["id"] as? Int
        self.assignedName = assigned["name"] as? String
        // PATH Fixed_version
        let fixed = dict["fixed_version"] as AnyObject
        self.fixedId = fixed["id"] as? Int
        self.fixedName = fixed["name"] as? String
        
    }
}
