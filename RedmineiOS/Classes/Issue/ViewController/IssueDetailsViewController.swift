//
//  IssueDetailsViewController.swift
//  RedmineiOS
//
//  Created by Nattawut Nokyoo on 2/9/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class IssueDetailsViewController: BaseTableViewController {
    
    var tracker: String? = ""
    var identifier: Int? = 0
    
    lazy var viewModel: IssueDetailViewModelProtocol = IssueDetailViewModel()
    let disposeBag = DisposeBag()
    var objects: IssueDetailsModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getIssueDetails()
        setupViewModel()
        self.tableView.delegate = self
    }

    func setupViewModel() {
        viewModel.success.subscribe(onNext: { [weak self] (success) in
//            self?.setupView()
//            self?.setupViewStatus()
//            self?.setupViewDetail()
        }).disposed(by: disposeBag)
    }
    
}

extension IssueDetailsViewController {
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var sectionTitle: String?
        if section == 0 {
            sectionTitle = "[\((tracker)!)] #\((identifier)!)"
        } else if section == 1 {
            sectionTitle = "ABOUT"
        } else if section == 2 {
            sectionTitle = "HISTORY"
        }
        return sectionTitle
    }
    
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    -> Int {
//        return 1
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: IssueDetailsViewCell.identifier, for: indexPath) as? IssueDetailsViewCell
//        let item = viewModel.data?.IssueDetails[indexPath.row]
//        cell?.subjectLabel.text = String(describing: "\((item?.subject ?? "")!)")
//        cell?.descriptionLabel.text = String(describing: "\((item?.descrip ?? "")!)")
//        return cell!
//    }
    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.tableView.deselectRow(at: indexPath, animated: true)
//    }
//
//    func setupView() {
//        self.navigationItem.title = self.viewModel.data?.projectName
//    }
//
//    func setupViewStatus() {
//        self.identifierLabel.text = String(describing: "#\((self.viewModel.data?.id) ?? 0)")
//        self.identifier = self.viewModel.data?.id ?? 0
//        self.statusLabel.text = String("\((self.viewModel.data?.statusName)!)")
//        self.tracker = String("\((self.viewModel.data?.trackerName)!)")
//        self.priorityLabel.text = String("\((self.viewModel.data?.priorityName)!)")
//        self.priorityLabel.textColor = setupText(dataPriorty: (self.viewModel.data?.priorityId)!)
//    }
//
//    func setupViewDetail() {
//        self.projectLabel.text = self.viewModel.data?.projectName
//        self.statusLabel.text = self.viewModel.data?.statusName
//        self.priorityLabel.text = self.viewModel.data?.priorityName
//        self.assigneeLabel.text = self.viewModel.data?.assignedName
//        self.doneLabel.text = String(describing: "\((self.viewModel.data?.done ?? 0)!)%")
//        self.startdateLabel.text = self.viewModel.data?.start//setupDate(dateTime: (self.viewModel.data?.start)!)
//        self.duedateLabel.text = self.viewModel.data?.due
//        self.targetLabel.text = self.viewModel.data?.fixedName
//        self.authorLabel.text = self.viewModel.data?.authorName
//        self.createdateLabel.text = setupDate(dateTime: (self.viewModel.data?.create)!)
//        self.lastupdateLabel.text = setupDate(dateTime: (self.viewModel.data?.update)!)
//        self.categoryLabel.text = "Empty"
//        self.estimatedLabel.text = String(describing: (self.viewModel.data?.estimatedHours) ?? 0)
//    }
//
//    func setupDate(dateTime: String) -> String{
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
//        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+7:00")
//        dateFormatter.locale = Locale.init(identifier: "en_US")
//        let date = dateFormatter.date(from: (dateTime ))
//        dateFormatter.dateFormat = "HH:mm:ss - d/MM/yyyy"
//        let newDate = dateFormatter.string(from: date!)
//        return newDate
//    }
//
//    func setupText(dataPriorty: Int) -> UIColor {
//        switch dataPriorty {
//            case 1: priorityLabel.textColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
//            case 2: priorityLabel.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
//            case 3: priorityLabel.textColor = #colorLiteral(red: 1, green: 0.5960784314, blue: 0, alpha: 1)
//            case 4: priorityLabel.textColor = #colorLiteral(red: 0.9019607843, green: 0.3176470588, blue: 0, alpha: 1)
//            case 5: priorityLabel.textColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0, alpha: 1)
//            default: priorityLabel.textColor = #colorLiteral(red: 0.3764705882, green: 0.4901960784, blue: 0.5450980392, alpha: 1)
//        }
//        return priorityLabel.textColor!
//    }
//
//}
}
