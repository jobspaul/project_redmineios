//
//  IssueViewController.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class IssueViewController: BaseTableViewController {
    
    var pid: Int? = 0
    var name: String? = ""
    
    lazy var viewModel: IssueViewModelProtocol = IssueViewModel()
    let disposeBag = DisposeBag()
    var objects: IssueModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.getIssue(pid: pid)
        setupViewModel()
        // MARK: TableViewDelegate
        self.tableView.delegate = self
        self.refreshControl?.addTarget(self, action: #selector(pullToRefresh), for: UIControlEvents.valueChanged)
        // MARK: Remove Back Title
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func setupViewModel() {
        viewModel.success.subscribe(onNext: { [weak self] (success) in
            self?.tableView.reloadData()
        }).disposed(by: disposeBag)
    }
    
    @objc func pullToRefresh() {
        viewModel.getIssue(pid: pid)
        if (self.refreshControl?.isRefreshing)!
        {
            self.refreshControl?.endRefreshing()
        }
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    @IBAction func actionNewIssue(_ sender: UIButton) {
        performSegue(withIdentifier: "goToNewIssue", sender: self)
    }
}

extension IssueViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    -> Int {
        return viewModel.data?.IssueList.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 114.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? IssueViewCell
        let item = viewModel.data?.IssueList[indexPath.row]
        cell?.subjectLabel.text = String(describing: "\((item?.subject ?? "")!)")
        cell?.identifierLabel.text = String(describing: "# \((item?.id ?? 0)!)")
        cell?.trackerLabel.text = String(describing: "\((item?.trackerName ?? "")!)")
        cell?.priorityLabel.text = String(describing: "\((item?.priorityName ?? "")!)")
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as? NewIssueTableViewController
        vc?.pid = pid
        vc?.name = name
    }
}
