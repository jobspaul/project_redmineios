//
//  IssueDetailsViewCell.swift
//  RedmineiOS
//
//  Created by tanut2539 on 3/5/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit

class IssueDetailsViewCell: UITableViewCell {
    
    static let identifier = "Cell"
    
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var identifierLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var projectLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var priorityLabel: UILabel!
    @IBOutlet weak var assigneeLabel: UILabel!
    @IBOutlet weak var doneLabel: UILabel!
    @IBOutlet weak var startdateLabel: UILabel!
    @IBOutlet weak var duedateLabel: UILabel!
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var createdateLabel: UILabel!
    @IBOutlet weak var lastupdateLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var estimatedLabel: UILabel!
    @IBOutlet weak var spentimeLabel: UILabel!
    @IBOutlet weak var parentLabel: UILabel!
    
}
