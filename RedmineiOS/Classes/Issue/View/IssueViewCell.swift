//
//  IssueViewCell.swift
//  RedmineiOS
//
//  Created by tanut2539 on 3/2/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit

class IssueViewCell: UITableViewCell {
    
    static let identifier = "Cell"
    
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var identifierLabel: UILabel!
    @IBOutlet weak var trackerLabel: UILabel!
    @IBOutlet weak var priorityLabel: UILabel!
    
}
