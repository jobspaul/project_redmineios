//
//  IssueViewModel.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import RxSwift
import RxCocoa

protocol IssueViewModelProtocol {
    var error: PublishSubject<String?> { get set }
    var success: PublishSubject<IssueModel?> { get set }
    func getIssue(pid: Int?)
    var data: IssueModel? { get set }
}

class IssueViewModel: IssueViewModelProtocol {

    var data: IssueModel?
    var error = PublishSubject<String?>()
    var success = PublishSubject<IssueModel?>()
    var limit: Int = 0
    var key: String? = ""
    var plistPathLoginKey: String!
    
    func getIssue(pid: Int?) {
        plistPathLoginKey = Account.sharedInstance.key
        do {
            let plistKey = try String(contentsOfFile: plistPathLoginKey)
            key = plistKey
        } catch {
            print(error)
        }
        let router = Router.getIssue(key: key, pid: pid, page: 0)
        _ = APIRequest.request(withRouter: router, withHandler: getHandler())
    }
    
    func getHandler() -> APIRequest.completionHandler {
        return { [weak self] (response, error) in
            if let response = response as? IssueModel {
                self?.data = response
                self?.success.onNext(response)
            } else {
                self?.error.onNext(error?.localizedDescription ?? "")
            }
        }
    }
    
}
