//
//  IssueDetailViewModel.swift
//  RedmineiOS
//
//  Created by Nattawut Nokyoo on 2/9/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import RxSwift
import RxCocoa

protocol IssueDetailViewModelProtocol {
    var error: PublishSubject<String?> { get set }
    var success: PublishSubject<IssueDetailsModel?> { get set }
    func getIssueDetails()
    var data: IssueDetailsModel? { get set }
}

class IssueDetailViewModel: IssueDetailViewModelProtocol {
    
    var data: IssueDetailsModel?
    var error = PublishSubject<String?>()
    var success = PublishSubject<IssueDetailsModel?>()
    var limit: Int = 0
    var key: String? = ""
    var plistPathLoginKey: String!
    
    func getIssueDetails() {
        plistPathLoginKey = Account.sharedInstance.key
        do {
            let plistKey = try String(contentsOfFile: plistPathLoginKey)
            key = plistKey
        } catch {
            print(error)
        }
        let router = Router.getIssueDetails(key: key, id: 16674, page: 0)
        _ = APIRequest.request(withRouter: router, withHandler: getHandler())
    }
    
    func getHandler() -> APIRequest.completionHandler {
        return { [weak self] (response, error) in
            if let response = response as? IssueDetailsModel {
                self?.data = response
                self?.success.onNext(response)
            } else {
                self?.error.onNext(error?.localizedDescription ?? "")
            }
        }
    }
    
}
