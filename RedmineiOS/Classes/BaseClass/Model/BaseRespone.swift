//
//  BaseRespone.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import Foundation

public class BaseModel: NSObject {
    
    var limit : Int? = 0
    var offset : Int? = 0
    var total: Int? = 0
    
    public required init(withDictionary dict: AnyObject) {
        self.limit = dict["limit"] as? Int
        self.offset = dict["offset"] as? Int
        self.total = dict["total_count"] as? Int
    }
}
