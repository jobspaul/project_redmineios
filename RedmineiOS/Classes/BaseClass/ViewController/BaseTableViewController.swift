//
//  BaseTableViewController.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

extension BaseTableViewController: BaseViewModelDelegate {
    
    public func showLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    public func hideLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    public func onDataDidLoad() {
        
    }
    
    public func onDataDidLoadErrorWithMessage(errorMessage: String) {
        
    }
}
