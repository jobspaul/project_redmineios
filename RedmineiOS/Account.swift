//
//  Account.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/27/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit

public class Account {
    
    public static let sharedInstance = Account()
    var pathforKey: String!
    var key: String = String()
    var id: String = String()
    
    func preparePlistKey(){
        let rootPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, .userDomainMask, true)[0]
        key = rootPath + "/Key.plist"
        if !FileManager.default.fileExists(atPath: key) {
            let plistPathInBundle = Bundle.main.path(forResource: "Key", ofType: "plist") as String!
            do {
                try FileManager.default.copyItem(atPath: plistPathInBundle!, toPath: key)
            }catch{
                print(error)
            }
        }
    }
    
    func preparePlistId(){
        let rootPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, .userDomainMask, true)[0]
        id = rootPath + "/Id.plist"
        if !FileManager.default.fileExists(atPath: id) {
            let plistPathInBundle = Bundle.main.path(forResource: "Id", ofType: "plist") as String!
            do {
                try FileManager.default.copyItem(atPath: plistPathInBundle!, toPath: id)
            }catch{
                print(error)
            }
        }
    }
    
}
