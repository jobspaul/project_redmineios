//
//  AppDelegateManager.swift
//  RedmineiOS
//
//  Created by tanut2539 on 2/26/18.
//  Copyright © 2018 Tanut Leelaparsert. All rights reserved.
//

import UIKit

public class AppDelegateManager {

    private let coveredViewTag: Int = 777666
    public static let sharedInstance = AppDelegateManager()
    
    // MARK: - Utils
    func topViewController(from controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let presented = controller?.presentedViewController {
            return topViewController(from: presented)
        }
        
        if let navigationController = controller as? UINavigationController {
            return topViewController(from: navigationController.visibleViewController)
        }
        
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(from: selected)
            }
        }
        
        return controller
    }
    
    private func splashScreenViewController(from viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        guard let viewController = viewController else { return nil }
        
        if let splashScreen = viewController as? SplashScreenViewController {
            return splashScreen
        } else if let viewController = topViewController(from: viewController) {
            return splashScreenViewController(from: viewController)
        }
        
        return nil
    }
    
    // MARK: - Covered View
    public func addCoveredView(toWindow window: UIWindow?) {
        guard let window = window else { return }
        if let coverView = getCoverView() {
            window.addSubview(coverView)
        } else {
            let blurView = getBlurView(withFrame: window.frame)
            window.addSubview(blurView)
        }
    }
    
    private func getCoverView() -> UIView? {
        let storyboardBundle = Bundle(for: SplashScreenViewController.self)
        let storyboard = UIStoryboard(name: "SplashScreen", bundle: storyboardBundle)
        if let viewController = storyboard.instantiateInitialViewController() {
            let coveredView = viewController.view
            coveredView?.tag = coveredViewTag
            return coveredView
        }
        return nil
    }
    
    private func getBlurView(withFrame frame: CGRect) -> UIView {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.tag = coveredViewTag
        blurEffectView.frame = frame
        return blurEffectView
    }
    
    public func removeCoveredView(fromWindow window: UIWindow?) {
        guard let window = window else { return }
        guard let coveredView = window.viewWithTag(coveredViewTag) else { return }
        coveredView.removeFromSuperview()
    }

}
